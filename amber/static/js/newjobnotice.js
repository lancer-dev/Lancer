$(document).on('ready', function() {
    $("#jobImages").fileinput({
        maxFileCount: 10,
        allowedFileTypes: ["image"],
        maxFileSize: 256,
        uploadExtraData: function() {
			return { jobname:String($("#jobname").val()), jobdesc:String($("#jobdesc").val())}
        },
        uploadUrl: "/upload"
    });
});

function clearTextButtonClicked() {
	document.getElementById("jobdesc").value = "";
	document.getElementById("jobname").value = "";
};