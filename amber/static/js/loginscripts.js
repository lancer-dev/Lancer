function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId());
    console.log("Name: " + profile.getName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
    $.post("/login", {id_id: profile.getId(), id_token: id_token, id_image: profile.getImageUrl(), id_name: profile.getName()}).done(function(data, textStatus, xhr){
    	switch(xhr.status){
    		case 205:
    			if (window.location.pathname != "/register") {
    				window.location.href = "/register";    				
    			};
    			break;
    		case 201:
    			window.location.reload();
    			break;
    	}	
    });
};
var revokeAllScopes = function() {
	auth2.disconnect();
}
