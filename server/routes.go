package server

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"updateinfo",
		"POST",
		"/updateinfo",
		UpdateInfo,
	},
	// Route{
	// 	"Logout",
	// 	"GET",
	// 	"/logout",
	// 	Logout,
	// },
	// For adding data to
	// the database
	Route{
		"AddData",
		"GET",
		"/data/{key}/{value}",
		AddData,
	},
	// To be able to
	// get data from
	// the database
	Route{
		"SeeData",
		"GET",
		"/show/{key}",
		ShowData,
	},
	// So we can see
	// that cookies
	// are working
	Route{
		"TestCookies",
		"GET",
		"/cookies/{url}",
		TestCookies,
	},
	Route{
		"ListJobs",
		"GET",
		"/listjobs",
		ListJobs,
	},
	// General catch-all
	// for loading in
	// websites
	Route{
		"LoadPage",
		"GET",
		"/{url}",
		LoadPage,
	},
	Route{
		"LoadPage",
		"GET",
		"/",
		LoadPage,
	},
	// Handles logins
	// Expects a google token
	Route{
		"Login",
		"POST",
		"/login",
		Login,
	},
	Route{
		"LoadJobNotice",
		"GET",
		"/jobnotice/{username}/{jobname}",
		LoadJobNotice,
	},
	Route{
		"Upload",
		"POST",
		"/upload",
		Upload,
	},
	Route{
		"AddUser",
		"POST",
		"/register",
		AddUser,
	},
}
