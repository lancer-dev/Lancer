package server

import (
	"encoding/json"
	"github.com/boltdb/bolt"
	"log"
	"net/http"
	"os"
)

func LoadGenericPage(data map[string]interface{}, url string, w http.ResponseWriter, r *http.Request) {
	if data == nil {
		data, _ = GetUser(map[string]interface{}{}, r, w)
	} else {
		data, _ = GetUser(data, r, w)
	}
	templ, ok := getTemplate(url)
	if !ok {
		templ, _ = getTemplate("404")
	}
	templ.Execute(w, data)

}

func InitDB(database string) error {
	db, err := bolt.Open("shield.db", 0600, nil)
	db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte(database))
		return nil
	})
	db.Close()
	return err
}

// Database is what bucket you're using
// Key and value are the values you want
// to be added.
func AddKeyValue(database, key, value string) error {
	db, err := bolt.Open("shield.db", 0600, nil)
	if err != nil {
		return err
	}

	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(database))
		err := b.Put([]byte(key), []byte(value))
		return err
	})

	db.Close()
	if err != nil {
		return err
	}
	return nil
}

// Does similiar to AddKeyValue,
// but gets values instead.
func GetKeyValue(database, key string, callback func(key, value string)) {
	db, err := bolt.Open("shield.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}

	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(database))
		v := b.Get([]byte(key))
		callback(key, string(v))
		return nil
	})
	db.Close()
}

// type Job struct {
// 	Description string   `json:"Description"`
// 	Media       []string `json:"Media"`
// }

// Black magic starts here
func GetAllValues(database string, start, stop int) ([]string, error) {
	var rv []string
	db, err := bolt.Open("shield.db", 0600, nil)
	if err != nil {
		return nil, err
	}
	if start == 0 && stop == 0 {
		db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(database))
			c := b.Cursor()
			count := 0
			for k, v := c.First(); k != nil; k, v = c.Next() {
				rv = append(rv, string(v))
				count++
			}
			defer db.Close()
			return nil
		})
	} else {
		db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(database))
			c := b.Cursor()
			count := 0
			c.First()
			for i := 0; i < start-1; i++ {
				c.Next()
			}
			for k, v := c.Next(); k != nil; k, v = c.Next() {
				rv = append(rv, string(v))
				count++
				if count == stop {
					defer db.Close()
					return nil
				}
			}
			return nil
		})
	}
	return rv, nil

}

func GetUser(data map[string]interface{}, r *http.Request, w http.ResponseWriter) (map[string]interface{}, bool) {
	session, _ := store.Get(r, "login")
	first := session.Values["image"]
	second := session.Values["name"]
	third := session.Values["login"]
	fsession, _ := store.Get(r, "session-store")
	flashes := fsession.Flashes()
	if first != nil && second != nil && third != nil {
		image, _ := first.(*Data)
		name, _ := second.(*Data)
		aud, _ := third.(*Data)
		data["Name"] = name.Info
		data["Userimage"] = image.Info
		data["UID"] = aud.Info
		if aud != nil {
			name, loggedin := IsLoggedIn(aud.Info)
			if loggedin {
				person := &User{}
				GetKeyValue("DataBucket", name, func(key, value string) {
					json.Unmarshal([]byte(value), &person)
				})
				data["Username"] = name
				data["Profilename"] = person.Profilename
				data["Location"] = person.Location
				data["Twitter"] = person.Twitter
			}
		}
		if len(flashes) > 0 {
			data["Flash"] = flashes[0].(string)
			// log.Println("Received flash " + flashes[0].(string) + "!")
			fsession.Save(r, w)
		}
		return data, true
	} else {

		if len(flashes) > 0 {
			data["Flash"] = flashes[0].(string)
			// log.Println("Received flash " + flashes[0].(string) + "!")
			fsession.Save(r, w)
		}
		return data, false
	}
}

func IsLoggedIn(token string) (string, bool) {
	var rv bool
	var username string
	GetKeyValue("DataBucket", token, func(key, value string) {
		if value != "" {
			username = value
			rv = true
		} else {
			username = ""
			rv = false
		}
	})
	return username, rv
}

func JobAlreadyExists(path string) (bool, error) {
	var rv bool
	GetKeyValue("JobBucket", path, func(path, value string) {
		if value != "" {
			rv = true
		} else {
			rv = false
		}
	})
	return rv, nil
}

func JobFolderExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
