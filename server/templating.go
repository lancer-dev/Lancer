package server

import (
	"github.com/eknkc/amber"
	"html/template"
	"log"
)

// Fetches the related data from the store of amberfiles
// and gives them to the user, making some error checking along
// the way, and returns a false as the second argument if something
// goes wrong
func getTemplate(url string) (*template.Template, bool) {
	if url == "" {
		url = "index"
	}

	var DefaultOptions = amber.Options{true, true}
	var DefaultDirOptions = amber.DirOptions{".amber", true}
	templates, err := amber.CompileDir("amber/", DefaultDirOptions, DefaultOptions)
	if err != nil {
		log.Print("templating.go - Failed to get amber files ")
		log.Print(err)
		log.Println()
	}
	template, ok := templates[url]
	if !ok {
		return nil, ok
	}
	return template, true // index.amber Go Template
}
