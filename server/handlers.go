package server

import (
	"encoding/gob"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	ourAud = "312249847388-f87o1eiv1cog04jrj240uc0nfjdnv4jr.apps.googleusercontent.com"
)

// To be able to get information
// from the cookies
func init() {
	gob.Register(&Data{})
	gob.Register(&M{})
	InitDB("DataBucket")
	InitDB("JobBucket")
	InitDB("Profile")
}

// So we can store information
// in sessions at the user
var store = sessions.NewCookieStore([]byte("secret-password"))

// Gives out a 404
func NotFound(w http.ResponseWriter, r *http.Request) {
	LoadGenericPage(nil, "404", w, r)
}

// Handles general calls
// to the website, as not to
// load something empty
func LoadPage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	url := vars["url"]
	if url == "" {
		url = "index"
	}
	LoadGenericPage(nil, url, w, r)
}

// Loads job notices.
// Uses the jobnotice template
// Gets jobdescription and images/video from database with {NAME} as in /jobnotice/{NAME}
func LoadJobNotice(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	jobname := vars["username"] + "/" + vars["jobname"]
	log.Printf("Loading job: %s\n", jobname)
	var job Job

	//Load the json describing the job from the database.
	GetKeyValue("JobBucket", jobname, func(_, value string) {
		//Unmarshal the json.
		err := json.Unmarshal([]byte(value), &job)
		if err != nil {
			log.Printf("Error unmarshaling job description: %s", err)
			return
		}
	})

	d := map[string]interface{}{
		"Description": job.Description,
		"Media":       job.Media,
		"JobName":     jobname,
	}

	//Execute the jobnotice template.
	LoadGenericPage(d, "jobnotice", w, r)

}

// For easily adding information
// to the database from
// the website.session, _ := store.Get(r, "login")
func AddData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]
	value := vars["value"]
	AddKeyValue("DataBucket", key, value)
	data := map[string]interface{}{
		"Key":   key,
		"Value": value,
	}
	LoadGenericPage(data, "addeddata", w, r)
}

func AddUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	var ok bool
	var multiple bool
	session, _ := store.Get(r, "login")
	token := session.Values["login"]
	if token == nil {
		flash, _ := store.Get(r, "session-store")
		flash.AddFlash("You must sign in with Google first!")
		LoadGenericPage(nil, "register", w, r)
	}
	data := token.(*Data)
	username := template.HTMLEscapeString(strings.TrimSpace(r.Form["username"][0]))
	ssn := template.HTMLEscapeString(strings.TrimSpace(r.Form["ssn"][0]))
	phone := template.HTMLEscapeString(strings.TrimSpace(r.Form["phone"][0]))
	GetKeyValue("DataBucket", username, func(key, value string) {
		if value != "" {
			ok = false
		} else {
			ok = true
		}
		return
	})

	GetKeyValue("DataBucket", data.Info, func(key, value string) {
		if value != "" {
			multiple = false
		} else {
			multiple = true
		}
		return
	})

	if !ok || !multiple {
		// log.Println("User already exists!")
		session, _ := store.Get(r, "session-store")
		if !ok {
			session.AddFlash("Username already exists!")
		} else {
			session.AddFlash("You can only register once.")
		}
		session.Save(r, w)
		LoadGenericPage(nil, "register", w, r)
	} else {
		entry := &User{
			Profilename: username,
			Phonenumber: phone,
			Ssn:         ssn,
		}

		login, _ := store.Get(r, "login")
		token := login.Values["login"]
		if token == nil {
			log.Println("Unable to get token.")
			return
		}
		info := token.(*Data)
		dbentry, _ := json.Marshal(entry)

		AddKeyValue("DataBucket", username, string(dbentry))
		AddKeyValue("DataBucket", info.Info, username)

		session, _ := store.Get(r, "session-store")
		session.AddFlash("Successfully created user!")
		session.Save(r, w)

		LoadGenericPage(nil, "profile", w, r)
	}
}

// Proof of concept that
// we can connect to the
// database and show information
// to the website from the server
func ShowData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["key"]
	GetKeyValue("DataBucket", key, func(key, value string) {
		data := map[string]interface{}{
			"Key":   key,
			"Value": value,
		}
		LoadGenericPage(data, "saveddata", w, r)
	})
}

// Simple way of testing cookies,
// this adds the last site you visited
// to the cookie, and reads it the next
// time you enter.
func TestCookies(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["url"]

	session, _ := store.Get(r, "session-name")

	tv := session.Values["site"]
	text, _ := tv.(*Data)

	data := map[string]interface{}{
		"Cookies": text.Info,
	}

	var sessionInfo = &Data{}
	sessionInfo.Info = key
	session.Values["site"] = sessionInfo
	session.Save(r, w)

	LoadGenericPage(data, "cookies", w, r)
}

// func Logout(w http.ResponseWriter, r *http.Request) {
// 	session, _ := store.Get(r, "login")
// 	token := session.Values["login"]
// 	if token == nil {
// 		session, _ = store.Get(r, "session-store")
// 		session.AddFlash("Already logged out!")
// 		LoadGenericPage(nil, "index", w, r)
// 		return
// 	}
// 	logout := token.(*Data)
// 	//log.Println("https://accounts.google.com/o/oauth2/revoke?token=" + logout.Info)
// 	resp, err := http.Get("https://accounts.google.com/o/oauth2/revoke?token=" + logout.Info)
// 	if err != nil {
// 		log.Println(err)
// 		log.Println(logout.Info)
// 	}
// 	if resp.StatusCode == 200 {
// 		session.Values["login"] = ""
// 		session.Values["name"] = ""
// 		session.Values["image"] = ""
// 		session, _ = store.Get(r, "session-store")
// 		session.AddFlash("You are now logged out!")
// 		session.Save(r, w)
// 		log.Println(resp.StatusCode)
// 		LoadGenericPage(nil, "index", w, r)
// 	} else {
// 		log.Println(resp.StatusCode)
// 		session, _ = store.Get(r, "session-store")
// 		session.AddFlash("Something went wrong, you are still logged in.")
// 		session.Save(r, w)
// 		LoadGenericPage(nil, "index", w, r)

// 	}

// }

func Login(w http.ResponseWriter, r *http.Request) {
	//Get the google token
	token := r.FormValue("id_token")
	name := r.FormValue("id_name")
	image := r.FormValue("id_image")
	gid := r.FormValue("id_id")
	//Verify the google token against google.
	session, _ := store.Get(r, "login")
	uid := session.Values["login"]
	uname := session.Values["name"]
	uimage := session.Values["image"]
	ugid := session.Values["id"]

	id := &Data{Info: ""}
	compname := &Data{Info: ""}
	compimage := &Data{Info: ""}
	googleid := &Data{Info: ""}
	// log.Println(uid)
	// log.Println(uname)
	// log.Println(uimage)
	// log.Println(ugid)
	if uid != nil && uname != nil && uimage != nil && ugid != nil {
		id = uid.(*Data)
		compname = uname.(*Data)
		compimage = uimage.(*Data)
		googleid = ugid.(*Data)
		// log.Println("Got cookie values.")
	}
	resp, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token)
	if err != nil {
		log.Printf("Error: %s", err)
	}
	if resp.StatusCode == 200 {
		//Get the 'aud' that google returns. This should match our websites registered aud.
		decoder := json.NewDecoder(resp.Body)
		var a Aud
		err = decoder.Decode(&a)
		// log.Println(name == compname.Info)
		// log.Println(image == compimage.Info)
		if err != nil {
			//log.Println("Failed to decode aud from google.")
		} else if token == id.Info && name == compname.Info && image == compimage.Info && gid == googleid.Info {
			// log.Println("User reauthenticated.")
			statuscode := 0
			GetKeyValue("DataBucket", id.Info, func(key, value string) {
				if value == "" {
					statuscode = 205
				} else {
					statuscode = 200
				}
			})
			w.WriteHeader(statuscode)
		} else {
			if a.Aud != ourAud {
				//log.Println("Aud from token does not match our Aud.")
			} else {
				//log.Println("User successfully authenticated.")
				session, _ := store.Get(r, "login")
				var login = &Data{}
				var username = &Data{}
				var userimage = &Data{}
				var egid = &Data{}
				egid.Info = gid
				session.Values["id"] = egid
				login.Info = token
				session.Values["login"] = login
				username.Info = name
				//log.Println("Adding username: " + username.Info)
				session.Values["name"] = username
				userimage.Info = image
				//log.Println("Adding userimage: " + userimage.Info)
				session.Values["image"] = userimage
				session.Save(r, w)
				w.WriteHeader(http.StatusCreated)
				//Do something with cookies
			}
		}
	} else {
		//log.Println("Token is not valid.")
	}
}

func Upload(w http.ResponseWriter, r *http.Request) {

	var filenames []string
	var filename string

	log.Println("Got post for new job notice!")

	//Get the users token.
	session, _ := store.Get(r, "login")
	token := session.Values["login"]
	if token == nil {
		log.Println("Unable to get token when submitting job.")
		w.Write([]byte("{error: 'Please login.'}"))
		return
	}
	data := token.(*Data)

	//Check if the user is logged in.
	username, loggedin := IsLoggedIn(data.Info)
	if !loggedin {
		log.Println("Non logged user tried to submit job.")
		w.Write([]byte("{error: 'Please login.'}"))
		return
	}

	//Get the jobname.
	jobname := r.FormValue("jobname")

	//Get the job description.
	jobdesc := r.FormValue("jobdesc")

	//Check if the jobname or jobdesription is empty.
	if jobname == "" || jobdesc == "" {
		log.Println("Aborting new job notice. The jobname or description was empty.")
		w.Write([]byte("{error: 'Jobname and Job Description must be filled in.'}"))
		return
	}

	//Add the username to the jobname.
	jobname = username + "/" + jobname

	//Check if the job already exists.
	//If it does, append the images to the existing job instead of creating a new one.
	exists, err := JobAlreadyExists(jobname)
	if err != nil {
		log.Println("Aborting job notice update/upload. Error when checking if job exists.")
		w.Write([]byte("{error: 'Failed to upload or update job. Please try again later.'}"))
		return
	}

	//Parse the form.
	err = r.ParseMultipartForm(100000)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
		return
	}

	//Get a reference to the parsed form.
	form := r.MultipartForm

	//Get the file headers.
	files := form.File["jobImages[]"]

	//Create the image directory for the job.
	//First check if the folder already exists (case for when multiple images are uploaded).
	path := "amber/static/jobImages/" + jobname
	folderexists, err := JobFolderExists(path)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
		return
	}
	if !folderexists {
		err = os.MkdirAll(path, 0700)
		if err != nil {
			log.Println(err)
			w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
			return
		}

	}

	for i, _ := range files {
		temp := path + "/" + files[i].Filename
		log.Printf("Creating image file: %s\n", temp)

		//For each file header, get a handle to the file it represents.
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			log.Println(err)
			w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
			return
		}

		//Create the destination file.
		dst, err := os.Create(path + "/" + files[i].Filename)
		defer dst.Close()
		if err != nil {
			log.Println(err)
			w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
			return
		}

		//Copy the uploaded image to the destination file.
		_, err = io.Copy(dst, file)
		if err != nil {
			log.Println(err)
			w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
			return
		}
		log.Println("File created.")
		filename = jobname + "/" + files[i].Filename
	}

	log.Println("Successfully saved images.")

	//If the job did not exist, create a job object. Otherwise get the existing job and update it.
	var j Job
	if !exists {
		filenames = append(filenames, filename)
		j = Job{
			Description: jobdesc,
			Name:        jobname,
			Media:       filenames,
		}
	} else {
		GetKeyValue("JobBucket", jobname, func(_, value string) {
			//Unmarshal the json.
			err := json.Unmarshal([]byte(value), &j)
			if err != nil {
				log.Printf("Error unmarshaling job description: %s", err)
				w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
				return
			}
			//Append the new image.
			j.Media = append(j.Media, filename)

			//Update the description.
			j.Description = jobdesc
		})
	}

	//Encode it to json.
	jobson, err := json.Marshal(j)

	log.Println("Storing job {0}: ", jobname)

	//Insert the job into the database with the keyname jobname and value jobson.
	err = AddKeyValue("JobBucket", jobname, string(jobson))
	if err != nil {
		log.Println(err)
		w.Write([]byte("{error: 'Failed to upload job. Please try again later.'}"))
		return
	}

	//Answer with empty json if ok.
	w.Write([]byte("{}"))

}

func UpdateInfo(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "login")
	token := session.Values["login"]
	data := token.(*Data)
	if data == nil {
		session, _ = store.Get(r, "session-store")
		session.AddFlash("You are not logged in!")
		LoadGenericPage(nil, "index", w, r)
	} else {
		name, ok := IsLoggedIn(data.Info)
		if !ok {
			session, _ = store.Get(r, "session-store")
			session.AddFlash("You are not logged in!")
			LoadGenericPage(nil, "index", w, r)
		}
		twitter := r.FormValue("twitter")
		location := r.FormValue("location")
		updateperson := &User{}
		GetKeyValue("DataBucket", name, func(_, value string) {
			json.Unmarshal([]byte(value), &updateperson)
		})
		updateperson.Twitter = twitter
		updateperson.Location = location
		entry, _ := json.Marshal(updateperson)
		AddKeyValue("DataBucket", name, string(entry))
		session, _ = store.Get(r, "session-store")
		session.AddFlash("Updated successfully!")
		LoadGenericPage(nil, "profile", w, r)
	}
}

func ListJobs(w http.ResponseWriter, r *http.Request) {
	//Define the list of jobs that will be sent to the template.
	var jobData []JobWithThumbnail

	//Get a list of jobs.
	list, err := GetAllValues("JobBucket", 0, 0)
	if err != nil {
		log.Println(err)
	}

	//For each job, unmarshal it and add it to the template data.
	for j := range list {
		var job Job
		err := json.Unmarshal([]byte(list[j]), &job)
		if err != nil {
			log.Printf("Error unmarshaling job description: %s", err)
		} else {
			if len(job.Description) > 100 {
				job.Description = job.Description[:100]
			}
			jwt := JobWithThumbnail{
				Description: job.Description + "...",
				Name:        job.Name,
				Thumb:       job.Media[0],
			}
			jobData = append(jobData, jwt)
		}
	}

	//Add the unmarshalled jobs to the template.
	d := map[string]interface{}{
		"Jobs": jobData,
	}

	//Serve the template.
	LoadGenericPage(d, "listjobs", w, r)

}
