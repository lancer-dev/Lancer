package server

// Sample struct
// to pass data
// to the cookies
type Data struct {
	Info string
}

// Json field we are interested in getting back from google when verifying a token.
type Aud struct {
	Aud string `json:"aud"`
}

type Job struct {
	Description string   `json:"Description"`
	Name        string   `json:"Name"`
	Media       []string `json:"Media"`
}

type JobWithThumbnail struct {
	Description string `json:"Description"`
	Name        string `json:"Name"`
	Thumb       string `json:"Thumb"`
}

type User struct {
	Profilename string
	Phonenumber string
	Ssn         string
	Location    string
	Twitter     string
}

type M map[string]interface{}
