package main

import (
	"lancer-dev/Lancer/server"
	"log"
	"net/http"
)

func main() {
	//Creates a new router that can be found in the server package,
	// handles the file server, allowing people to get css files, js etc.
	// and handles the case if there's no handler.
	router := server.NewRouter()
	router.StrictSlash(true)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("amber/static/"))))
	router.NotFoundHandler = http.HandlerFunc(server.NotFound)

	log.Fatal(http.ListenAndServe(":8080", router))
}
